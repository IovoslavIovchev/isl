//! This file contains the full `isl` syntax by example.
//! Firstly, `//!` represents a doc-comment.
//!
//! Standard C-style comments are supported - `//` and `/* ... */`.

SOME_RANDOM_GLOBAL_CONST :: const u32 = 0xFF0A;

//! statics are mutable by default
SOME_RANDOM_GLOBAL_STATIC :: static u32 = 0xFF0A;

Vec2 :: struct
{
    x: i32;
    y: i32;
}

Numbers :: enum(u8)
{
    one = 1,
    two,
}

// is 1 byte in size
Untagged :: union
{
    _byte: u8;
    _bool: bool;
}

// is 2 bytes in size
Tagged :: variant(u8)
{
    Byte(u8),
    Bool(bool),
}

//! `range`s always take the smallest possible size that can contain
//! the difference between the range's upper and lower bounds.
//! e.g. this is 1 byte in size
Single :: range 0..10 - 1;

//! functions can be generics, but aren't currently supported
// id :: <T> :: (x: T): T { return x; }

//! structs can be generic too; again, not currently supported
// GenericArray :: <T, const Size> :: struct
// {
//     x: Size[T];
// }

//! unions, variants can be generic too
//! enums, however, cannot be generic

//! `main` is implicitly `pub`
main :: (): i32
{
    status: mut i32 = 0;

    // at some point this should be possible
    // status: mut _ = 0i32;

    // `defer`s are executed in reversed order
    defer {
        // this is the last thing that will execute before the function returns
        status = 0;
    };

    // this is the second to last thing that will execute before the function returns
    defer status = 1;

    // a mutable pointer, can change the value it references but cannot be reassigned
    status_ptr: *mut i32 = &status;

    // a mutable mutable pointer, can be reassigned and can change the value it references
    mut_status_ptr: mut *mut i32 = &status;

    whatever: bool = true && false || true;
    byte_array: 5[u8] = [0, 1, 2, 3, 4];

    // array pointers contain a pointer and the length of the referenced portion of the array
    ptr_to_byte_array: *[u8] = &byte_array;

    // statics can be mutated freely
    SOME_RANDOM_GLOBAL_STATIC = 0;

    {
        // assigning to `status` via a pointer
        &status_ptr = -3;
    }

    if false
    {
        // not gonna happen anyway
        return -1;
    }
    else if false || false {/* this won't happen either */}

    // since blocks and ifs are envisioned as expressions, this will be possible at some point
    // x: isize = if 5 % 2 == 0 {
    //     x: _ = 9;
    //
    //     5 + 8 + x
    // } else { 3 } + { 6 };

   return status;
}
