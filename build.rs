fn main() {
    lalrpop::process_root().expect("lalrpop");

    println!("cargo:rerun-if-changed=src/isl.lalrpop");
}
