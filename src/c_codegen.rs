use crate::ast;

use std::{
    collections::{HashMap, HashSet, VecDeque},
    fmt::{write, Write},
};

#[derive(Debug)]
pub struct Gen<'a> {
    mod_name: &'a str,
    mod_src: &'a str,
    module: &'a ast::Module,

    indent: u64,
    is_executable: bool,
    out: String,
}

impl<'a> Gen<'a> {
    pub fn new(mod_name: &'a str, mod_src: &'a str, module: &'a ast::Module) -> Self {
        Self {
            mod_name,
            mod_src,
            module,
            indent: 0,
            is_executable: false,
            out: String::new(),
        }
    }

    pub fn generate(mut self) -> (String, bool) {
        self.gen_primitives();
        for item in self.module.0.iter() {
            let ident = self.span_to_str(item.ident).to_owned();
            use ast::ItemKind::*;
            match &item.kind {
                CInclude(span) => self.gen_c_include(*span),
                FnDecl(header, generics, block) => {
                    if generics.is_none() {
                        self.gen_fn(&ident, &header, block.as_ref())
                    }
                }
                Struct(fields, generics) => {
                    if generics.is_none() {
                        todo!()
                    }
                }
                _ => todo!(),
            }
        }

        (self.out, self.is_executable)
    }

    fn span_to_str(&self, (l, r): ast::Span) -> &str {
        &self.mod_src[l..r]
    }

    fn gen_primitives(&mut self) {
        write!(
            &mut self.out,
            r#"#ifndef ISL_PRIMITIVES
#define ISL_PRIMITIVES

typedef int          __isl_i32;
typedef unsigned int __isl_u32;

/* TODO */

#endif

"#
        )
        .unwrap();
    }

    fn gen_span(&mut self, (l, r): ast::Span) {
        let span = &self.mod_src[l..r];
        write!(&mut self.out, "{}", span).unwrap();
    }

    fn gen_c_include(&mut self, span: ast::Span) {
        self.gen_span(span);
        write!(&mut self.out, "\n").unwrap();
    }

    fn gen_fn(&mut self, ident: &str, head: &ast::FnHeader, block: Option<&ast::Block>) {
        match head.ret_ty.as_ref() {
            Some(ty) => self.gen_type(ty),
            _ => write!(&mut self.out, "void").unwrap(),
        }

        write!(&mut self.out, "\t{}\t(", ident).unwrap();
        if ident == "main" {
            self.is_executable = true;
        }

        for (i, param) in head.params.iter().enumerate() {
            self.gen_type(&param.ty);
            write!(&mut self.out, "  ",).unwrap();
            self.gen_span(param.ident);
            write!(
                &mut self.out,
                "{}  ",
                if i != head.params.len() - 1 {
                    " , "
                } else {
                    ""
                }
            )
            .unwrap();
        }

        if let Some(block) = block {
            write!(&mut self.out, ") {{\n").unwrap();
            self.indent += 1;

            if let Some(ty) = head.ret_ty.as_ref() {
                self.gen_indent();
                self.gen_type(ty);
                write!(&mut self.out, " _isl_return_val;\n\n").unwrap();
            }

            self.gen_block(block);

            if let Some(ty) = head.ret_ty.as_ref() {
                self.gen_indent();
                write!(&mut self.out, "\n_isl_return:\n").unwrap();

                // TODO: any defers go here

                self.gen_indent();
                write!(&mut self.out, "return _isl_return_val;\n").unwrap();
            }

            self.indent -= 1;
            write!(&mut self.out, "}}\n").unwrap();
        } else {
            write!(&mut self.out, ");\n").unwrap();
        }
    }

    fn gen_block(&mut self, block: &ast::Block) {
        for stmt in block.stmts.iter() {
            self.gen_stmt(stmt);
        }
    }

    fn gen_indent(&mut self) {
        for i in 0..self.indent {
            write!(&mut self.out, "\t").unwrap();
        }
    }

    fn gen_decl(&mut self, decl: &ast::Decl) {
        if !decl.mutable {
            write!(&mut self.out, "const\t").unwrap();
        }

        self.gen_type(decl.ty.as_ref().unwrap());
        write!(&mut self.out, "\t").unwrap();
        self.gen_span(decl.ident);
        write!(&mut self.out, "\t=\t").unwrap();
        if let Some(val) = decl.val.as_ref() {
            self.gen_expr(val);
        }
        write!(&mut self.out, ";\n").unwrap();
    }

    fn gen_stmt(&mut self, stmt: &ast::Statement) {
        self.gen_indent();

        use ast::StatementKind::*;
        match &stmt.kind {
            Return(e) => {
                if let Some(e) = e {
                    write!(&mut self.out, "_isl_return_val = ").unwrap();
                    self.gen_expr(e);
                    write!(&mut self.out, ";\tgoto _isl_return; ").unwrap();
                }
            }
            Semi(e) => {
                self.gen_expr(e);
                write!(&mut self.out, " ;\n").unwrap();
            }
            Decl(decl) => self.gen_decl(decl),
            x => {
                dbg!(x);
                todo!();
            }
        }
    }

    fn gen_expr(&mut self, expr: &ast::Expr) {
        use ast::ExprKind::*;
        match &expr.kind {
            Literal(l) => self.gen_lit(l),
            Call(id, args) => {
                self.gen_span(*id);
                write!(&mut self.out, " (").unwrap();
                for (i, arg) in args.iter().enumerate() {
                    self.gen_expr(arg);
                    write!(
                        &mut self.out,
                        "{}",
                        if i != args.len() - 1 { " , " } else { "" }
                    )
                    .unwrap();
                }
                write!(&mut self.out, ")").unwrap();
            }
            StructInit(path, initializers) => {
                write!(&mut self.out, "\n").unwrap();
                self.indent += 1;
                self.gen_indent();
                write!(&mut self.out, "/* initialize ").unwrap();
                self.gen_path(path);
                write!(&mut self.out, " */ {{\n").unwrap();
                self.indent += 1;
                // TODO: sort the initializers to match the way the struct fields are generated
                for (i, (ident, expr)) in initializers.iter().enumerate() {
                    self.gen_indent();
                    write!(&mut self.out, "/* ").unwrap();
                    self.gen_span(*ident);
                    write!(&mut self.out, " = */\t").unwrap();
                    self.gen_expr(expr);
                    if i != initializers.len() - 1 {
                        write!(&mut self.out, ",").unwrap();
                    }
                    write!(&mut self.out, "\n").unwrap();
                }
                self.indent -= 1;
                self.gen_indent();
                write!(&mut self.out, "}}").unwrap();
                self.indent -= 1;
            }
            Path(p) => self.gen_path(p),
            x => {
                dbg!(x);
                todo!()
            }
        }
    }

    fn gen_lit(&mut self, lit: &ast::Literal) {
        use ast::LitKind::*;
        match &lit.kind {
            Int(i, _) => write!(&mut self.out, "{}", i).unwrap(),
            Str(s) => self.gen_span(s.span),
            _ => todo!(),
        }
    }

    fn gen_type(&mut self, ty: &ast::Type) {
        use ast::TypeKind::*;
        match &ty.kind {
            Path(p) => self.gen_path(p),
            _ => todo!(),
        }
    }

    fn gen_path(&mut self, path: &ast::Path) {
        let ty = self.span_to_str(path.span).to_owned();
        match ty.as_str() {
            "i32" => write!(&mut self.out, "__isl_i32").unwrap(),
            "u32" => write!(&mut self.out, "__isl_u32").unwrap(),
            ty => {
                write!(&mut self.out, "{}", ty).unwrap();
                if let Some(args) = path.args.as_ref() {
                    for arg in args.iter() {
                        write!(&mut self.out, "__").unwrap();
                        self.gen_type(arg);
                    }
                }
            }
        }
    }
}
