#![feature(generators, generator_trait)]

#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(pub isl);

mod ast;
pub mod c_codegen;
mod validation;

use std::{
    collections::{HashMap, HashSet, VecDeque},
    path::{Path, PathBuf},
};

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

pub fn parse_program(entry: &str) -> Result<ast::Program> {
    let mut parsed_files = HashSet::new();
    let mut modules = HashMap::new();
    let mut parse_queue = VecDeque::new();

    let entry_path = std::fs::canonicalize(entry).unwrap();
    let base = entry_path.parent().unwrap();

    parse_queue.push_back(entry.to_owned());

    let mut parser = isl::ModuleParser::new();

    while let Some(next) = parse_queue.pop_front() {
        let next = std::fs::canonicalize(next)?.to_str().unwrap().to_owned();

        if parsed_files.contains(&next) {
            continue;
        }

        println!("> Parsing `{}`", &next);

        let src = std::fs::read_to_string(&next)?;
        let r#mod = // resolution::resolve_mod(
            parser.parse(&src).expect("parsing module");
        //); // TODO

        let items = &r#mod.0;
        items
            .iter()
            .filter_map(|item| match &item.kind {
                // TODO: optimize
                ast::ItemKind::Include(path) => {
                    let (l, r) = *path;
                    let name = span_to_str((l + 1, r - 1), &src);

                    if name == "std" {
                        // Hardcode the path here
                        dbg!("std");
                        None
                    } else {
                        let mut buf = PathBuf::from(Path::new(&next).parent().unwrap());
                        buf.push(name);
                        let path = buf.to_str().expect("Invalid path").to_owned();

                        Some(path)
                    }
                }
                _ => None,
            })
            .for_each(|path| parse_queue.push_back(path));

        parsed_files.insert(next.clone());
        modules.insert(next, (src, r#mod, modules.is_empty()));
    }

    Ok(ast::Program(modules))
}

pub(crate) fn path_to_mod_path(path: impl AsRef<Path>, base: impl AsRef<Path>) -> String {
    assert_eq!("isl", path.as_ref().extension().unwrap());

    let path = path.as_ref().strip_prefix(base).unwrap();

    path.components()
        .map(|x| {
            Path::new(&x)
                .file_stem()
                .unwrap()
                .to_str()
                .unwrap()
                .to_owned()
        })
        .collect::<Vec<_>>()
        .join("::")
}

pub(crate) fn span_to_str(span: ast::Span, src: &str) -> &str {
    let (start, end) = span;
    &src[start..end]
}
