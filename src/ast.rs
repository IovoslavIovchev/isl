// Legend:
// #ambig - should be an Expr but lalrpop doesn't support ambiguous grammar yet

use std::collections::HashMap;

#[derive(Debug)]
pub struct Program(
    /// e.g. `("foo::bar", (contents of "./foo/bar.isl", parsed module, is the entry module))`
    pub HashMap<String, (String, Module, bool)>,
);

#[derive(Debug, Default)]
pub struct Module(pub Vec<Item>);

/// (beginning index, ending index)
pub type Span = (usize, usize);

pub type Ident = Span;

// the base for most types here
#[derive(Debug)]
pub struct Spanned<Kind> {
    pub span: Span,
    pub kind: Kind,
}

#[derive(Debug)]
pub struct Item {
    pub span: Span,
    pub ident: Ident,
    pub vis: Visibility,
    pub kind: ItemKind,
}

#[derive(Debug)]
pub enum ItemKind {
    /// e.g. `"../other/some_mod"`
    Include(Span),
    /// A verbatim `#include ...`
    CInclude(Span),
    Const(Type, Box<Expr>),
    Static(Type, bool, Box<Expr>),
    FnDecl(FnHeader, Option<Generics>, Option<Block>),
    Struct(Vec<Field>, Option<Generics>),
    Enum(Vec<(Ident, Option<Expr>)>, Option<Type>),
    Union(Vec<Field>, Option<Generics>),
    Variant(Vec<(Ident, Option<Type>)>, Option<Generics>, Option<Type>),
    Impl(Option<Generics>, Vec<Item>),
    Range(Box<Expr>, Box<Expr>, Option<Type>),

    // TEMP, remove asap
    ExternConst(Path, Type),
    ExternStatic(Path, Type),
    ExternFn(Path, FnHeader),
}

#[derive(Debug)]
pub struct Path {
    pub span: Span,
    pub args: Option<Vec<GenericArg>>,
}

pub type GenericArg = Type;

pub type Type = Spanned<TypeKind>;

#[derive(Debug)]
pub enum TypeKind {
    Path(Path),
    Pointer(Box<Type>, bool),
    Array(Box<Type>, AnonConst),
    ArrayPointer(Box<Type>),
    SelfPointer(bool),

    Inferred, // unused for now
}

#[derive(Debug)]
pub struct Field {
    // TODO: attributes
    pub span: Span,
    pub vis: Visibility,
    pub ident: Ident,
    pub ty: Type,
}

#[derive(Debug)]
pub struct FnHeader {
    pub params: Vec<Param>,
    /// `None` equals `void`
    pub ret_ty: Option<Type>,
}

#[derive(Debug)]
pub struct Param {
    // TODO: attrs
    pub ident: Span,
    pub ty: Type,
}

#[derive(Debug)]
pub struct Block {
    pub span: Span,
    pub stmts: Vec<Statement>,
}

pub type Visibility = Spanned<VisKind>;

#[derive(Debug)]
pub enum VisKind {
    Pub,
    Priv,
}

pub type Statement = Spanned<StatementKind>;

#[derive(Debug)]
pub enum StatementKind {
    // #ambig
    Assign(Box<Expr>, Span, Box<Expr>),

    // #ambig
    /// A return statement with an optional return value
    Return(Option<Box<Expr>>),

    /// An `if` statement with a possible side effect
    If(Box<Expr>, Box<Block>, Option<Box<Statement>>),

    /// A block with a possible side effect
    Block(Block),

    /// A non-`;`-terminated expression
    Expr(Expr),

    /// A `;`-terminated expression
    Semi(Expr),

    Decl(Decl),

    Defer(Box<Statement>),

    /// e.g. just a `;`
    Empty,
}

#[derive(Debug)]
pub struct Decl {
    pub mutable: bool,
    pub ident: Span,
    pub ty: Option<Type>,
    pub val: Option<Expr>,
}

#[derive(Debug)]
pub struct AnonConst(pub Expr);

#[derive(Debug)]
pub struct Expr {
    pub span: Span,
    pub kind: ExprKind,
    // TODO: attributes
}

#[derive(Debug)]
pub enum ExprKind {
    Literal(Literal),
    Path(Path),
    Unary(UnOp, Box<Expr>),
    Binary(Box<Expr>, BinOp, Box<Expr>),
    Call(Span, Vec<Expr>),
    Array(Vec<Expr>),
    Index(Box<Expr>, Box<Expr>),
    StructInit(Path, Vec<(Ident, Expr)>),

    // TODO: support if and block expressions
    If(Box<Expr>, Box<Expr>, Box<Expr>),
    // Block(Block),
}

#[derive(Debug)]
pub enum UnOp {
    Pos,
    Neg,
    Inv,
    Not,
    AddrOf,
}

#[derive(Debug)]
pub enum BinOp {
    Sum,
    Sub,
    Mul,
    Div,
    Mod,
    And,
    Or,
    BoolAnd,
    BoolOr,
    Xor,
    Shl,
    Shr,
    Eq,
    Ne,
    Lt,
    Le,
    Gt,
    Ge,
}

#[derive(Debug)]
pub struct Generics {
    pub span: Span,
    pub params: Vec<GenericParam>,
}

#[derive(Debug)]
pub enum GenericParam {
    Type(Ident),
    Const(Span, Ident),
}

pub type Literal = Spanned<LitKind>;

#[derive(Debug)]
pub enum LitKind {
    Str(StrLit),
    Int(u128, LitIntKind),
}

#[derive(Debug)]
pub struct StrLit {
    pub span: Span,
    // something else?
}

#[derive(Debug)]
pub enum LitIntKind {
    Unanotated,
    Signed(IntType),
    Unsigned(UIntType),
}

#[derive(Debug)]
pub enum IntType {
    I8,
    I16,
    I32,
    I64,
    ISize,
}

#[derive(Debug)]
pub enum UIntType {
    U8,
    U16,
    U32,
    U64,
    USize,
}
