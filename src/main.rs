fn main() -> Result<(), Box<dyn std::error::Error>> {
    use lalrpop_util::ParseError::*;

    let entry = std::env::args().nth(1).unwrap_or_else(|| {
        println!("WARNING: Using fallback file");
        "syntax.isl".to_owned()
    });

    let program = ::isl::parse_program(&entry).unwrap();

    let mut res = None::<(String, bool)>;
    for (mod_name, v) in program.0 {
        let (mod_src, module, is_entry) = &v;
        if !is_entry {
            continue;
        }

        let (generated, is_exe) = ::isl::c_codegen::Gen::new(&mod_name, mod_src, module).generate();

        dbg!(mod_name, v);
        println!("=== Generated ===\n{}", generated);

        res = Some((generated, is_exe));
        break;
    }

    let (generated, is_exe) = match res {
        Some(x) => x,
        _ => return Ok(()),
    };

    use std::fs;
    if fs::metadata("./generated").is_ok() {
        fs::remove_dir_all("./generated").unwrap();
    }
    fs::create_dir_all("./generated").unwrap();
    fs::write("./generated/out.c", generated.as_bytes()).unwrap();

    let mut cmd = std::process::Command::new("clang-10");
    cmd.arg("-o")
        .arg("./generated/a.out")
        .arg("./generated/out.c");

    let cmd = if !is_exe { cmd.arg("-c") } else { &mut cmd };
    let out = cmd
        .current_dir(env!("CARGO_MANIFEST_DIR"))
        .output()
        .unwrap();
    let stderr = String::from_utf8(out.stderr).unwrap();

    println!("=== Stderr ===\n{}", stderr);

    // match ::isl::isl::ModuleParser::new().parse(&src) {
    //     Ok(ast) => {
    //         //dbg!(&ast);

    //         //let (module_name, compiled_out) = isl::ModuleCompiler::new(&src, "entry")
    //         //    .compile(ast)
    //         //    .unwrap();

    //         //println!("C output for {}:\n\n{}", module_name, compiled_out);
    //     }
    //     Err(e) => match e {
    //         UnrecognizedToken { token, expected: _ } => {
    //             let (start, _, end) = token;
    //             let left = &src[..start];
    //             let left_lines = left.lines().count();
    //             let line_start = left.rfind('\n').unwrap() + 1;
    //             let line_end = src[line_start..].find('\n').unwrap() + line_start;
    //             let (start, _end) = (start - line_start, end - line_start);
    //             let line = &src[line_start..line_end];
    //             let line_n = left_lines.to_string();
    //             let fmtd = format!("{}| {}", line_n, line);
    //             let mut err_line = String::with_capacity(fmtd.len());
    //             err_line.push_str(&" ".repeat(line_n.len() + 2 + start));
    //             err_line.push('^');
    //             eprintln!("Unrecognized token");
    //             eprintln!("{}", fmtd);
    //             eprintln!("{}", err_line);
    //         }
    //         e => {
    //             dbg!(e);
    //         }
    //     },
    // }

    Ok(())
}
