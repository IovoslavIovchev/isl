Wrapper :: <T> :: struct
{
    inner: T;
}

main :: (): i32
{
    wrapped: Wrapper.(i8) = Wrapper .{ inner = 5, };
    return 0;
}
