# isl

just an isle in the sea

# Overview

`isl` is a simple C-like language that aims to be minimalistic. The full syntax can be found in the `syntax.isl` file.
